# Le dev dont vous êtes le héros

## Préambule

Vous êtes au début de votre grande aventure de développeur. Ce terrain de jeu est immense, un monde ouvert où la seule limite est votre imagination !

Avec l'aide de votre mentor, le dénommé "Lelexxx, le pourfendeur de prod", vous trouverez des compagnons fidèles, vous vous équiperez de défenses solides et deviendrez maître dans l'art de la magie afin de braver le terrible "Donjon de la mise en production".

En route pour ce voyage extraordinaire qui vous mènera jusque dans l'antre de "MEP" !

## Mon avatar

Alexandre Ruiz (<rruiz.alex@gmail.com>) aka Lelexxx, Tech Lead .NET chez Apside depuis trois ans, j'encadre des équipes dans la conception et le développement de solutions logicielles (application web, API).

En parallèle, en tant qu'enseignant en JavaScript à l'IUT informatique de Clermont-Ferrand, je partage mes connaissances pour former les étudiants aux meilleures pratiques du développement web.

Vous pouvez me retrouver sur mes différents réseaux sociaux :

- https://alex-developpeur.fr/
- https://twitter.com/l_lexxx
- https://github.com/lelexxx

## Trouver des compagnons

Avant de vous lancer dans votre aventure, vous devez former une équipe solide avec laquelle partager votre travail et vos compétences pour aller plus loin !

1. Appliquer le GitFlow
2. Pair programming
3. Code Review

## Préparer ses défenses

Pour mener à bien votre quête, vous allez devoir protéger vos arrières avec une bonne armure et vous assurer de maintenir vos compétences des sorts.

1. Votre bouclier : UI Testing
2. Votre armure : API Testing
3. Votre livre de sorcellerie : Unit Testing

## La magie de la CI/CD

Et pour clôturer votre apprentissage, vous allez devoir  utiliser la magie à bon escient afin qu'elle vous assiste dans vos tâches du quotidien pour vous faciliter la vie.

1. Le scan de son code au Sonar
2. L'automatisation des tests
3. Le déploiement en continu

## MEP, le boss final

Nous y voilà, notre équipe d'aventuriers se retrouve en face à face avec "MEP", dit "Le vendredi de la terreur", le boss final de notre donjon !

Armés de courage, arborant leurs meilleurs équipements, nos héros se jettent dans le combat **pour en sortir victorieux** !
